#!/bin/bash

args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

#Get sql arguments
SQL_ARGS=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)

#read current database version
currentType=$(mysql $SQL_ARGS --protocol=tcp --execute="select buildType from versionInformation" --silent --skip-column-names)
if [ "$?" != "0" ]; then
	echo "Local database Type check fail; exiting.."
	exit 1
fi
echo $currentType


