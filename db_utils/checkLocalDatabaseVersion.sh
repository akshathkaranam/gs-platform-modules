#!/bin/bash

args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

#Get sql arguments
SQL_ARGS=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)


#read current database version
currentVersion=$(mysql --protocol=tcp $SQL_ARGS --execute="select concat(major,minor,build) from versionInformation" --silent --skip-column-names)
if [ "$?" != "0" ]; then
	echo "Local database version check fail; exiting.."
	exit 1
fi
echo $currentVersion


