#!/bin/bash
args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)
#echo $DB_UTILS_PATH

. $DB_UTILS_PATH/commandLineParser.sh $args

version=$(ls $parsed_Database_Path/$parsed_DatabaseName/db_versions/$parsed_DatabaseType/ | sort -r -n | awk '{printf "%03d", $1; exit}')
echo $version

if [ "$version" == "" ] || [ $version -lt 1 ]
then
	echo "no db found, exiting"
	exit 1;
fi

