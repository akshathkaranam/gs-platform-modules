#!/bin/bash

#Returns the arguments to be used for dumpsnapshot as it uses different directory for snapshot when run in update database

args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

DUMP_SNAPSHOT_ARGS=$($DB_UTILS_PATH/getArgumentsWithoutVersion.sh $args)


#Get other arguments excluding snapshot
if [ $parsed_Database_Version_Present == "YES" ]
then
    DUMP_SNAPSHOT_ARGS=$DUMP_SNAPSHOT_ARGS" -v$parsed_Database_Version"
fi

echo $DUMP_SNAPSHOT_ARGS