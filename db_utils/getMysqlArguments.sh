#!/bin/bash

#Returns the arguments to be used for mysql command depending upon the params passed
#If argument 1 i.e. $1 is passed as inc-database then database param is included otherwise it is excluded

MYSQL_ARG=

if [ $parsed_MySql_HostName_Present == "YES" ]
then
    MYSQL_ARG=$MYSQL_ARG" -h$parsed_MySql_HostName"
fi

if [ $parsed_MySql_PortNum_Present == "YES" ]
then
    MYSQL_ARG=$MYSQL_ARG" -P$parsed_MySql_PortNum"
fi

if [ $parsed_DBUserName_Present == "YES" ]
then
    MYSQL_ARG=$MYSQL_ARG" -u$parsed_DBUserName"
fi

if [ $parsed_DBPassword_Present == "YES" ]
then
    MYSQL_ARG=$MYSQL_ARG" -p$parsed_DBPassword"
fi

if [ $parsed_DatabaseName_Present == "YES" ] && [ "$1" == "inc-database" ]
then
    MYSQL_ARG=$MYSQL_ARG" -D$parsed_DatabaseName"
fi

#Sometimes arguments are required for passing between scripts in this case return with -d instead of -D
if [ $parsed_DatabaseName_Present == "YES" ] && [ "$1" == "inc-database-for-arg" ]
then
MYSQL_ARG=$MYSQL_ARG" -d$parsed_DatabaseName"
fi


echo $MYSQL_ARG