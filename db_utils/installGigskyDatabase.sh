#!/bin/bash

args=$@
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

if [ "$parsed_Database_Version_Present" == "NO" ]
then
    echo "Incorrect Arguments: Provide version number to be upgraded with -v option"
exit 1;
fi

DB_TARGET_PATH="$parsed_Database_Path/$parsed_DatabaseName/"

BASE_ARGUMENTS_TO_MYSQL_COMMAND=$($DB_UTILS_PATH/getMysqlArguments.sh)" --default-character-set=utf8 --protocol=tcp"
BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)" --default-character-set=utf8 --protocol=tcp"


#Directory from where to read insert data
BASE_PATH=$DB_TARGET_PATH/"db_versions/"$parsed_DatabaseType/$parsed_Database_Version
INSERT_DIR=$BASE_PATH/tableEntriesForInsert/

#check if directories exist
if [ ! -d "$BASE_PATH" ]
then
echo "$BASE_PATH doesn't exist. Please pass -e[dev/live/stage] -v [version] correctly"
exit 1
fi 

if [ ! -d "$INSERT_DIR" ]
then
echo "$INSERT_DIR doesn't exist. Please pass -e[dev/live/stage] -v [version] correctly"
exit 1
fi 


if [ ! -f "$BASE_PATH/workbench/workBenchExportedScript.sql" ]
then
echo "$BASE_PATH/workbench/workBenchExportedScript.sql doesn't exist. Please pass e[dev/live/stage] -v [version] correctly"
exit 1
fi

echo "creating DB users";
if [ ! -f "$DB_TARGET_PATH/createUser.sql" ]
then
    echo "$DB_TARGET_PATH/createUser.sql doesn't exist"
exit 1
fi

#exit if database already exists; fail
echo "Testing Database connection.."
mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SELECT 1" --silent --skip-column-names > /dev/null 2>&1
if [ "$?" == "0" ]; then
    echo "Database already exists; exiting.."
exit 1
fi


mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND < $DB_TARGET_PATH/createUser.sql

if [ "$?" != "0" ]; then
    echo "create user failed"
    exit 1;

fi



echo "users created with ALL privileges"
mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND < $BASE_PATH/workbench/workBenchExportedScript.sql
echo "database created"
#compose SQL Script file for upgrade
TEMPARORY_SQL_FILE_NAME=temperary_sql_file_name_1111.sql
rm -f $TEMPARORY_SQL_FILE_NAME
echo "set autocommit=0;" >> $TEMPARORY_SQL_FILE_NAME
echo "begin;" >> $TEMPARORY_SQL_FILE_NAME
echo "SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;" >> $TEMPARORY_SQL_FILE_NAME


#Load all data from csv files present in insert_data folder
CSV_FILE_LIST=$(ls $INSERT_DIR/*.csv | xargs -n1 basename)
echo "List of csv files from:" $INSERT_DIR
echo $CSV_FILE_LIST
echo "" ""
for CSV_FILE in $CSV_FILE_LIST
do
    TABLE_NAME=${CSV_FILE//.csv/}  # remove .csv from csv file 
    echo "inserting $CSV_FILE into table $TABLE_NAME"

    echo "LOAD DATA LOCAL INFILE '$INSERT_DIR/$CSV_FILE' REPLACE INTO TABLE $TABLE_NAME  FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 LINES;" >> $TEMPARORY_SQL_FILE_NAME
done

echo "SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;" >> $TEMPARORY_SQL_FILE_NAME
echo "commit;" >> $TEMPARORY_SQL_FILE_NAME


mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME < $TEMPARORY_SQL_FILE_NAME

if [ "$?" != "0" ]; then
	echo "" " "
	echo "****Insert failed "
	mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND --execute="drop schema $parsed_DatabaseName"
	rm -f $TEMPARORY_SQL_FILE_NAME
	exit 1
fi

rm -f $TEMPARORY_SQL_FILE_NAME

orphanChildRows=$($DB_UTILS_PATH/checkReferentialIntegrity.sh $args)
if [ "$?" != "0" ] || [ "$orphanChildRows" != "" ]; then
	echo "Referential Integrity Failed, $orphanChildRows, Exiting.."
	mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND --execute="drop schema $parsed_DatabaseName"
	exit 1
fi


echo "Testing Database creation.."
mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SELECT 1"
echo "-Successfully created-"

#mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SELECT @@tx_isolation"
#upgrade successful; configure right isolation level
mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SET  GLOBAL  TRANSACTION ISOLATION LEVEL  READ COMMITTED"
mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SELECT @@tx_isolation"

