#!/bin/bash


args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

if [ "$parsed_Database_Version_Present" == "NO" ]
    then
    echo "Incorrect Arguments: Provide version number to be upgraded with -v option"
    exit 1;
fi

if [[ -z "$parsed_DatabaseName" ]]
then
    echo "Incorrect Arguments: Provide db name with -d option"
    exit 1;
fi


if [[ -z "$parsed_Database_Path" ]]
then
    echo "Path to DB folder not specified through -c; defaults to current directory"
	parsed_Database_Path=./
fi


DB_TARGET_PATH="$parsed_Database_Path/$parsed_DatabaseName/db_versions/$parsed_DatabaseType/$parsed_Database_Version/"
echo DB_TARGET_PATH:$DB_TARGET_PATH


#check if directories exist
if [ ! -d "$DB_TARGET_PATH" ]
then
    echo "$DB_TARGET_PATH doesn't exist. Please pass -e[dev/live/stage] -v [version] correctly"
    exit 1
fi 

if [ ! -f "$DB_TARGET_PATH/workbench/workBenchExportedScript.sql" ]
then
    echo "$DB_TARGET_PATH/workbench/workBenchExportedScript.sql doesn't exist. Please pass e[dev/live/stage] -v [version] correctly"
    exit 1
fi

if [ ! -d "$parsed_Snapshot_Directory" ]
then
echo "$parsed_Snapshot_Directory is invalid. Please pass correct directory with -s option"
exit 1
fi


#Get sql arguments
SQL_ARGS_WITH_DATABASE=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)" --default-character-set=utf8"
SQL_ARGS=$($DB_UTILS_PATH/getMysqlArguments.sh)" --default-character-set=utf8"

$DB_UTILS_PATH/deleteLocalDatabase.sh $args
echo "Gigsky database dropped"

mysql $SQL_ARGS < $DB_TARGET_PATH/workbench/workBenchExportedScript.sql
echo "Gigsky database created"

#compose SQL Script file for upgrade
TEMPARORY_SQL_FILE_NAME=$DB_TARGET_PATH/temperary_sql_file_name_1111.sql
rm -f $TEMPARORY_SQL_FILE_NAME
echo "set autocommit=0;" >> $TEMPARORY_SQL_FILE_NAME
echo "begin;" >> $TEMPARORY_SQL_FILE_NAME
echo "SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;" >> $TEMPARORY_SQL_FILE_NAME


#Load all data from csv files present in insert_data folder
CSV_FILE_LIST=$(ls $parsed_Snapshot_Directory/*.csv | xargs -n1 basename)
echo "Inserting csv files from: $parsed_Snapshot_Directory" "" ""
echo $CSV_FILE_LIST
echo "" ""
for CSV_FILE in $CSV_FILE_LIST
do
    TABLE_NAME=${CSV_FILE//.csv/}  # remove .csv from csv file 
    #echo "inserting $CSV_FILE into table $TABLE_NAME"
    echo "LOAD DATA LOCAL INFILE '$parsed_Snapshot_Directory/$CSV_FILE' REPLACE INTO TABLE $TABLE_NAME  FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 LINES;" >> $TEMPARORY_SQL_FILE_NAME
done

echo "SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;" >> $TEMPARORY_SQL_FILE_NAME
echo "commit;" >> $TEMPARORY_SQL_FILE_NAME
#cat $TEMPARORY_SQL_FILE_NAME


mysql $SQL_ARGS_WITH_DATABASE < $TEMPARORY_SQL_FILE_NAME

if [ "$?" != "0" ]; then
	echo "" " "
	echo "****Insert failed "
	rm -f $TEMPARORY_SQL_FILE_NAME
	exit 1
fi

rm -f $TEMPARORY_SQL_FILE_NAME

orphanChildRows=$($DB_UTILS_PATH/checkReferentialIntegrity.sh $args)
if [ "$?" != "0" ] || [ "$orphanChildRows" != "" ]; then
	echo "Referential Integrity Failed, $orphanChildRows exiting.."
	exit 1
fi


echo "Testing Database creation.."
mysql $SQL_ARGS_WITH_DATABASE --execute="SELECT 1"
echo "-Successfully created-"

