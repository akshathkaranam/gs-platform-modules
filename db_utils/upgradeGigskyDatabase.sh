#!/bin/bash

#Parase the command line arugments. Since commandLineParser.sh is called in the same sheell i.e. . ./ $@ will contain nothing as commandLineParser.sh parases all arguments. Hence store them in temp variable before calling.

args=$@

#getPath of db_utils. It is in same directory as this file
UPDATE_GIGSKY_PATH=$(cd "$(dirname "$0")"; pwd)
DB_UTILS_PATH=$UPDATE_GIGSKY_PATH

echo UPDATE_GIGSKY_PATH:$UPDATE_GIGSKY_PATH
echo DB_UTILS_PATH:$DB_UTILS_PATH

. $DB_UTILS_PATH/commandLineParser.sh $args


if [ "$(whoami)" != "root" ]
then
	echo run as sudo, exiting
	exit 1;
fi

if [[ -z "$parsed_DatabaseName" ]]
then
    echo "Incorrect Arguments: Provide db name with -d option"
    exit 1;
fi


if [[ -z "$parsed_Database_Path" ]]
then
    echo "Path to DB folder not specified through -c; defaults to current directory"
	parsed_Database_Path=./
fi



if [ "$parsed_Database_Version_Present" == "NO" ]
then
    echo "Incorrect Arguments: Provide version number to be upgraded with -v option"
    exit 1;
fi


DB_TARGET_PATH="$parsed_Database_Path/$parsed_DatabaseName/"
export DB_TARGET_PATH;

PASSWORD_HASH_DIR="$DB_UTILS_PATH/passwordHashes"

echo DB_TARGET_PATH:$DB_TARGET_PATH

BASE_ARGUMENTS_TO_MYSQL_COMMAND=$($DB_UTILS_PATH/getMysqlArguments.sh)
BASE_ARGUMENTS_TO_MYSQL_COMMAND="$BASE_ARGUMENTS_TO_MYSQL_COMMAND --protocol=tcp"
BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)
BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME="$BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --protocol=tcp"


echo "SQL Command="$BASE_ARGUMENTS_TO_MYSQL_COMMAND
echo "SQL Command with db="$BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME


if [ "$parsed_DatabaseType" == "dev" ] 
then
	passwordHashFile=$PASSWORD_HASH_DIR/devDbPassword.hash
elif [ "$parsed_DatabaseType" == "live" ] 
then
	passwordHashFile=$PASSWORD_HASH_DIR/liveDbPassword.hash
elif [ "$parsed_DatabaseType" == "stage" ]
then
    passwordHashFile=$PASSWORD_HASH_DIR/stageDbPassword.hash
else
	echo "Error: Invalid arguments: choose dev / live / stage"
	exit 1
fi



#Authentication
$DB_UTILS_PATH/generateHash.sh $parsed_DatabaseType_Password | cmp -b $passwordHashFile > /dev/null 2>&1
if [ "$?" != "0" ] 
then
	echo "Authentication failed"
	exit 1
fi



CREATE_DBSCRIPT="createGigskyDatabase.sh"
UPGRADE_DBSCRIPT="upgradeGigskyDatabase.sh"
CREATE_SCRIPT_FOLDER_NAME="tableEntriesForCreate"
LIVE_SYNC_UPDATE_SCRIPT="liveSyncScript.sh"
DB_START_VERSION=100

#IF database does not exist
#	check from latest version to last for createScript
#	Not found; exit
#	createdVersion = call <version>/create

#else
#	createdVersion = checkLocalDatabaseVersion

dbCreatedVersion="0"
mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SELECT 1" --silent --skip-column-names > /dev/null 2>&1;
if [ "$?" != "0" ]
then
	echo "Database unreachable; creating.."

	for	((i=$parsed_Database_Version;	i>=$DB_START_VERSION; i--))
	do
        #Get arguments without version num, then give version number depending upon based on loop indes
        createDirectoryParams=$($DB_UTILS_PATH/getArgumentsWithoutVersion.sh $args)
		createDirectory=$DB_TARGET_PATH/"db_versions/"$parsed_DatabaseType/$i
		#echo createDirectory:$createDirectory;
		if	[ -d "$createDirectory/$CREATE_SCRIPT_FOLDER_NAME" ]
		then
			echo "create db from with params:"$createDirectoryParams -v$i 
			$DB_UTILS_PATH/$CREATE_DBSCRIPT $createDirectoryParams -v$i
            if [ "$?" != "0" ]
            then
                echo "create db failed"
                exit 1;
            fi
			dbCreatedVersion=$i

			echo dbCreatedVersion:$dbCreatedVersion;
			break;
		fi
	done

	if [ $dbCreatedVersion = "0" ] 
	then
		echo no db version found containing create db script
		exit 1;
	fi

else

    dbCreatedVersion=$($DB_UTILS_PATH/checkLocalDatabaseVersion.sh $args)

    #on store db upgrade is supported only from 110 version
    if [ $dbCreatedVersion -le 100 ] && [ "$parsed_DatabaseName" == "gigStore" ]
    then
        echo gigStore database upgrade supported from version 110 onwards
        exit 1
    fi

    #check whether we are upgrading over correct dev/live/stage
    #in gigsky database databasetype is added from 120 version onwards
    localBuildType=$($DB_UTILS_PATH/checkLocalDatabaseType.sh $args)
    if [	"$localBuildType" != "$parsed_DatabaseType" ]
    then
        echo "$dbCreatedVersion $localBuildType $parsed_DBUserName"
        #if it is gigsky schema then allow upgrading till 120 version
        if [ $dbCreatedVersion -lt 120 ] && [ "$parsed_DatabaseName" == "gigsky" ]
        then
            echo "Not doing database type check since version is $dbCreatedVersion and less than 120"
        else
            echo "Exiting as upgrading db type mismatch i.e. local is $localBuildType where as we are upgrading $parsed_DatabaseType"
            exit 1
        fi
    fi
	

	#initial version of db do not hv versionInformation
	if [ "$?" != "0" ] || [ "$dbCreatedVersion" == "" ]
	then
		echo Warn: Database Version Not Found
		dbCreatedVersion=$DB_START_VERSION
	fi

    echo "****dbCreatedVersion:$dbCreatedVersion"

fi

echo upgradeTo:$parsed_Database_Version createdVersion:$dbCreatedVersion
#check for upgrade to higher version
if [ $parsed_Database_Version -le $dbCreatedVersion ]
then
	echo database deployed version:$dbCreatedVersion is latest
	exit;
fi


#From localVersion + 1 till LatestVersion
#	call <version>/upgrade
ARGS_WITHOUT_VERSION_AND_SNAPSHOT=$($DB_UTILS_PATH/getArgumentsWithoutVersion.sh $args)
echo $ARGS_WITHOUT_VERSION_AND_SNAPSHOT

lastUpgradedVersion=$dbCreatedVersion;
for	((i=$dbCreatedVersion+1;	i<=parsed_Database_Version; i++))
do
	upgradeDirectory=$DB_TARGET_PATH/"db_versions/"$parsed_DatabaseType/$i
	if	[	-d	"$upgradeDirectory"	]
	then

        #dump the existing snapshot for rollback / recovery
        snapshotDirectory=$parsed_Snapshot_Directory/$parsed_DatabaseName/"db_versions/"$parsed_DatabaseType/$lastUpgradedVersion/tableEntriesForCreate

        #dump the current snapshot
        echo Dumping server db snapshot to $snapshotDirectory


        #$DB_UTILS_PATH/dumpDBSnapshot.sh $ARGS_WITHOUT_VERSION_AND_SNAPSHOT -s $snapshotDirectory
        #if [ "$?" != "0" ]
            #then
            #echo "Snapshot dump to:$snapshotDirectory failed. So only upgraded till $lastUpgradedVersion"
            #exit 1
        #fi

        #For dev/staging versions we will have to sync up certain files with live. so do it for only dev version
        #if [ "$parsed_DatabaseType" == "dev" ] || [ "$parsed_DatabaseType" == "stage" ]
        #then
        #    echo "Calling sync with live:"
        #    #call the live update script
        #    $DB_UTILS_PATH/$LIVE_SYNC_UPDATE_SCRIPT $ARGS_WITHOUT_VERSION_AND_SNAPSHOT -v $i
        #    if [ "$?" != "0" ]
        #    then
        #        echo "Update to Version $i Failed"
#
        #        echo "Rolling back to $lastUpgradedVersion with snapshot from $snapshotDirectory"
        #        $DB_UTILS_PATH/rollbackGigskyDatabase.sh $ARGS_WITHOUT_VERSION_AND_SNAPSHOT -s $snapshotDirectory -v $lastUpgradedVersion
#
        #        echo "Rolled back to $lastUpgradedVersion with snapshot from $snapshotDirectory as upgrade to version $i failed"
        #        echo "Exiting Upgrade"
        #        exit 1
        #    fi
        #fi


		if	[ -f "$upgradeDirectory/$UPGRADE_DBSCRIPT" ]
		then
			echo "upgrade directory:"$upgradeDirectory

            export DB_UTILS_PATH

			$upgradeDirectory/$UPGRADE_DBSCRIPT $ARGS_WITHOUT_VERSION_AND_SNAPSHOT -s $snapshotDirectory -v $parsed_Database_Version

			if [ "$?" != "0" ] 
			then
				echo "Update to Version $i Failed"

				#echo "Rolling back to $lastUpgradedVersion with snapshot from $snapshotDirectory"
				#$DB_UTILS_PATH/rollbackGigskyDatabase.sh $ARGS_WITHOUT_VERSION_AND_SNAPSHOT -s $snapshotDirectory -v $lastUpgradedVersion

                #echo "Rolled back to $lastUpgradedVersion with snapshot from $snapshotDirectory as upgrade to version $i failed"
				echo "Exiting Upgrade"
				exit 1
			fi
			lastUpgradedVersion=$i
			echo $lastUpgradedVersion
		else
			echo upgrade script not found in directory $upgradeDirectory, exiting
			exit 1
		fi

	fi
done

#mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SELECT @@tx_isolation"
#upgrade successful; configure right isolation level
mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SET  GLOBAL  TRANSACTION ISOLATION LEVEL  READ COMMITTED"
mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME --execute="SELECT @@tx_isolation"

