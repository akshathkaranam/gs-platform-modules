package com.gigsky.gscommon;

public enum GsImsiErrorCode {
    //request related error messages
    INVALID_LOGIN_CREDENTIALS(1000),
    REQUEST_FROM_UNTRUSTED_SOURCE(1001),
    INVALID_TOKEN(1002),

    //general errors
    INVALID_ARGUMENTS(1500),
    OPERATION_FAILED(1501),

    //imsi & msisdn related
    IMSI_MSISDN_ALREADY_EXISTS(1502),
    IMSI_MSISDN_NOT_EXIST(1503),

    //subscriber related
    SUBSCRIBER_ALREADY_EXISTS(1504),
    SUBSCRIBER_NOT_EXIST(1505),
    SUBSCRIBER_COUNTER_NOT_FOUND(1507),
    SUBSCRIBER_COUNTER_NOT_SET(1508),

    //event notification related
    EVENT_NOTIFICATION_URL_ALREADY_REGISTERED(1506),

    //others/unknown/unhandled
    UNKNOWN_ERROR(2000),

    //resource not found
    RESOURCE_NOT_FOUND(3000);

    public int statusCode;

    GsImsiErrorCode(int errorCode) {
        this.statusCode = errorCode;
    }

    public static GsImsiErrorCode getErrorCodeByErrorInt(int errorCode) {
        for(GsImsiErrorCode gsImsiErrorCode : values()) {
            if(gsImsiErrorCode.statusCode == errorCode) {
                return gsImsiErrorCode;
            }
        }
        throw new IllegalArgumentException(errorCode + " is not a valid GS IMSI Server error code");
    }
}