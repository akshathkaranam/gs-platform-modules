package com.gigsky.gscommon.config;

import java.math.BigDecimal;

public interface ConfigurationProvider {
    public int getValueInt(BaseConfig config, int aDefaultValue);

    public long getValueLong(BaseConfig config, long aDefaultValue);

    public String getValueString(BaseConfig config, String aDefaultValue);

    public long getValueMilliSecs(BaseConfig config, String aDefaultValue);

    public BigDecimal getValueBigDecimal(BaseConfig config, BigDecimal aDefaultValue);

    public boolean getValueBoolean(BaseConfig config, boolean aDefaultValue);

    public String getDecryptedStringValue(BaseConfig config, String aDefaultValue);
}
