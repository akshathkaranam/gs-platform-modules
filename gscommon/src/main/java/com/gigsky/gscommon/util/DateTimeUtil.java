package com.gigsky.gscommon.util;

import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtil {
    private static final int MILLI_SEC_DECIMALS = 0;
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String UTC_TIME_ZONE_ID = "UTC";
    public static final TimeZone UTC_TIME_ZONE = TimeZone.getTimeZone(UTC_TIME_ZONE_ID);


    public static Calendar getUTCCalendar(Date timestampToBeConverted) {
        return convertToTimeZone(timestampToBeConverted, UTC_TIME_ZONE);
    }

    public static Calendar convertToTimeZone(Date timestampToBeConverted, String targetTimeZone) {
        return convertToTimeZone(timestampToBeConverted, TimeZone.getTimeZone(targetTimeZone));
    }

    public static Calendar convertToTimeZone(Date timestampToBeConverted, TimeZone targetTimeZone) {
        if (timestampToBeConverted == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(targetTimeZone);
        calendar.setTimeInMillis(timestampToBeConverted.getTime());
        return calendar;
    }

    public static String getUTCFormattedDateString(Calendar calendarToBeConverted, String inputDateFormat) {
        if (calendarToBeConverted == null) {
            return null;
        }

        SimpleDateFormat dateFormat = null;
        if(StringUtils.isNotEmpty(inputDateFormat)) {
            dateFormat = new SimpleDateFormat(inputDateFormat);
        }
        else {
            dateFormat = new SimpleDateFormat(DATE_FORMAT);
        }

        dateFormat.setTimeZone(UTC_TIME_ZONE);
        return dateFormat.format(calendarToBeConverted.getTime());
    }

    public static String getUTCFormattedDateString(Calendar calendarToBeConverted) {
        if (calendarToBeConverted == null) {
            return null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        dateFormat.setTimeZone(UTC_TIME_ZONE);
        return dateFormat.format(calendarToBeConverted.getTime());
    }

    public static String getUTCFormattedDateStringFromTimestamp(Timestamp timestampToBeConverted) {
        return getUTCFormattedDateString(getUTCCalendar(timestampToBeConverted), DATE_FORMAT);
    }

    public static String getUTCFormattedDateStringFromTimestamp(Timestamp timestampToBeConverted, String dateFormat) {
        return getUTCFormattedDateString(getUTCCalendar(timestampToBeConverted), dateFormat);
    }

    public static Date convertFromUTCToTargetTimeZone(String inputUTCDateString, String targetTimeZone) {
        if (StringUtils.isEmpty(inputUTCDateString)) {
            return null;
        }
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            dateFormat.setLenient(false);
            dateFormat.setTimeZone(UTC_TIME_ZONE);
            Date convertedUTCDate = dateFormat.parse(inputUTCDateString);
            if (StringUtils.isNotEmpty(targetTimeZone)) {
                dateFormat.setTimeZone(TimeZone.getTimeZone(targetTimeZone));
                return dateFormat.parse(dateFormat.format(convertedUTCDate));
            }
            return dateFormat.parse(dateFormat.format(convertedUTCDate));
        }
        catch (ParseException pe) {
            throw new IllegalArgumentException(inputUTCDateString + " is invalid date");
        }
    }

    public static Date parseDate(String inputDate, String dateFormat) {
        if(StringUtils.isNotEmpty(inputDate) && StringUtils.isNotEmpty(dateFormat)) {
            try {
                SimpleDateFormat format = new SimpleDateFormat(dateFormat);
                return format.parse(inputDate);
            }
            catch (ParseException pe) {
                throw new IllegalArgumentException(inputDate + " is not in " + dateFormat + " format");
            }
        }
        return null;
    }

    public static Date parseDateWithTimeZone(String inputDate, String dateFormat, TimeZone timeZone) {
        if(StringUtils.isNotEmpty(inputDate) && StringUtils.isNotEmpty(dateFormat)) {
            try {
                SimpleDateFormat format = new SimpleDateFormat(dateFormat);
                format.setTimeZone(timeZone);
                return format.parse(inputDate);
            }
            catch (ParseException pe) {
                throw new IllegalArgumentException(inputDate + " is not in " + dateFormat + " format");
            }
        }
        return null;
    }

    public static String convertFromDBToFormattedTargetTimeZone(Timestamp timestamp, TimeZone targetTimezone, String targetFormat){
        SimpleDateFormat dateFormat = new SimpleDateFormat(targetFormat);
        dateFormat.setTimeZone(targetTimezone);
        return dateFormat.format(new Date(timestamp.getTime()));
    }

    public static String truncateMilliSeconds(String updateTime) {
        if (updateTime.indexOf(".") > 0) {
            int lastIndex = (updateTime.lastIndexOf(".") + MILLI_SEC_DECIMALS > updateTime.length()) ? updateTime.length() : (updateTime.lastIndexOf(".") + MILLI_SEC_DECIMALS);
            updateTime = updateTime.substring(0, lastIndex);
        }
        return updateTime;
    }
}
