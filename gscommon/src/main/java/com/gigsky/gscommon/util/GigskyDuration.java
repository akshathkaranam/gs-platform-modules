package com.gigsky.gscommon.util;

public class GigskyDuration {
    public static final String INFINITE = "-1";
    private int year;
    private int month;
    private int day;
    private int hours;
    private int minutes;
    private int seconds;
    private int milliSeconds;

    private boolean infinite = false;

    public GigskyDuration(String aValue) {
        //-1 means infinite
        Long d = new Long(aValue);
        if (d == -1) {
            infinite = true;
            this.year = -1;
            this.month = -1;
            this.day = -1;
            this.hours = -1;
            this.minutes = -1;
            this.seconds = -1;
            this.milliSeconds = -1;
            return;
        }

        //10 digit
        //YY-MM-DD-HH-MM
        this.year = Integer.valueOf(aValue.substring(0, 2));
        this.month = Integer.valueOf(aValue.substring(2, 4));
        this.day = Integer.valueOf(aValue.substring(4, 6));
        this.hours = Integer.valueOf(aValue.substring(6, 8));
        this.minutes = Integer.valueOf(aValue.substring(8, 10));
        this.seconds = Integer.valueOf(aValue.substring(10, 12));
        this.milliSeconds = Integer.valueOf(aValue.substring(12, 15));
    }

    /**
     * This method converts the GigskyDuration to seconds, note that this method ignores the digits specified in milli seconds.
     *
     * @param aDuration GigskyDuration to be converted to seconds
     * @return seconds for GigskyDuration
     */
    public static long convertDurationToSeconds(GigskyDuration aDuration) {
        long durationInMilliSeconds = aDuration.convertTimeInMilliSeconds();
        if (durationInMilliSeconds != -1) {
            return (durationInMilliSeconds / 1000);
        }
        return durationInMilliSeconds;
    }

    public String getMySQLTimeStamp() {
        if (infinite) {
            return null;
        }

        String lExpiryDate = "now() ";
        lExpiryDate += " + INTERVAL " + getYear() + " YEAR";
        lExpiryDate += " + INTERVAL " + getMonth() + " MONTH";
        lExpiryDate += " + INTERVAL " + getDay() + " DAY";
        lExpiryDate += " + INTERVAL " + getHours() + " HOUR";
        lExpiryDate += " + INTERVAL " + getMinutes() + " MINUTE";
        lExpiryDate += " + INTERVAL " + getSeconds() + " SECOND";
        lExpiryDate += " + INTERVAL " + (getMilliSeconds() * 1000) + " MICROSECOND";
        return lExpiryDate;
    }

    public long convertTimeInMilliSeconds() {
        if (this.infinite) {
            return -1;
        }
        long milliSeconds = getMilliSeconds();

        long t = getSeconds();
        milliSeconds += (t * 1000);

        t = getMinutes();
        milliSeconds += (t * 60 * 1000);

        t = getHours();
        milliSeconds += (t * 60 * 60 * 1000);

        t = getDay();
        milliSeconds += (t * 24 * 60 * 60 * 1000);

        t = getMonth();
        milliSeconds += (t * 31 * 24 * 60 * 60 * 1000);

        t = getYear();
        milliSeconds += (t * 365 * 31 * 24 * 60 * 60 * 1000);

        return milliSeconds;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getMilliSeconds() {
        return milliSeconds;
    }

    public void setMilliSeconds(int milliSeconds) {
        this.milliSeconds = milliSeconds;
    }

    public boolean isInfinite() {
        return infinite;
    }

    public void setInfinite(boolean infinite) {
        this.infinite = infinite;
    }

    @Override
    public String toString() {
        return "Duration[" +
                year + " Years, " +
                month + " Months, " +
                day + " Days, " +
                hours + " Hours, " +
                minutes + " Minutes, " +
                seconds + " Seconds, " +
                milliSeconds + " MilliSeconds]";
    }
}