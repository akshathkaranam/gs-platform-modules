package com.gigsky.gscommon.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vishaljogi on 14/05/19.
 */
public class Logging {
    private static final String splunkLoggerTag = "splunk";

    private static final Logger LOGGER = LoggerFactory.getLogger(splunkLoggerTag);

    public static void failedLogging(String operation, String log,Object... objects){
        LOGGER.error("Operation: " + operation + ", Result: FAILED, " + log,objects);
    }
    public static void failedLogging(String server, int tenantsId, String operation, String log,Object... objects){
        LOGGER.error("Server: " + server + "TenantsId:"+ tenantsId +",Operation: " + operation + ", Result: FAILED, " + log,objects);
    }
    public static void successLogging(String operation, String log,Object... objects){
        LOGGER.info("Operation: " + operation + ", Result: SUCCESSFUL, " + log,objects);
    }
    public static void successLogging(String server, int tenantsId, String operation, String log,Object... objects){
        LOGGER.info("Server: " + server + "TenantsId:"+ tenantsId +", Operation: " + operation + ", Result: SUCCESSFUL, " + log,objects);
    }
    public static void successLogging(String log,Object... objects){
        LOGGER.info(log,objects);
    }
}
