package com.gigsky.gscommon.util.encryption;

/**
 * Do not modify this file.
 * <p/>
 * Utilities for hashing..sensitive information.
 *
 * @author rchouhan
 */
public interface EncryptionFactory {
    /**
     * There can only be only be one encryptor per version for a web app.
     *
     * @param aVersion
     * @return GigSkyEncryptor specified by version
     */
    GigSkyEncryptor getEncryptor(String aVersion);
}
