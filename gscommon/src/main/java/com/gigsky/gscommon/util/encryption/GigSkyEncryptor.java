package com.gigsky.gscommon.util.encryption;

/**
 * An interface for general version class.
 *
 * @author rchouhan
 */
public interface GigSkyEncryptor {

    /**
     * decrypt a string.
     */
    public String decryptString(String aInputString);

    /**
     * encrypts a string.
     */
    public String encryptString(String aInputString);

    /**
     * get a merged string which can be hashed.
     */
    public String getStringHashForUser(String aUserName, String aPassword);

    /**
     * gets hash string for given input
     */
    public String hashString(String aInput);

    /**
     * compare and find if the raw (plain text ) aInputToCheck string hash is equal to aHashedValue.
     */
    public boolean compare(String aUserName, String aPassWord, String aHashedValue);

    /**
     * This is to compare input and hashed values with new version of the hashing algorithm.
     */
    public boolean compare(String aInput, String aHashedValue);
}