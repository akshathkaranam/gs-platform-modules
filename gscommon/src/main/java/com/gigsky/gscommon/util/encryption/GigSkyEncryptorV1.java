package com.gigsky.gscommon.util.encryption;

import com.sun.jersey.core.util.Base64;
import org.jasypt.digest.StandardStringDigester;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.salt.RandomSaltGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * first generation gigsky encryptor class.
 *
 * @author rchouhan
 */
@Component("V1_Encryptor")
public class GigSkyEncryptorV1 implements GigSkyEncryptor {

    public static final String M_VERSION = "1.0.0";
    public static final int M_NO_ITERATION = 1000;
    public static final String M_ALGO_NAME = "PBEWithMD5AndTripleDES";
    public static final String M_HASH_ALGO_NAME = "SHA-1";
    public static final String M_STATIC_FIELDS = "iUnxLYFOf2Z/maA1z54KUx6T3BxqPJqQjeR7og==;";
    private static final Logger logger = LoggerFactory.getLogger(GigSkyEncryptorV1.class);

    /**
     * print a byte array using characters.
     *
     * @param aBuffer
     */
    private static void printByteArray(byte[] aBuffer) {
        for (int i = 0; i < aBuffer.length; i++) {
            System.out.println("\n" + " " + i + " char is=" + (char) aBuffer[i]);
        }
    }

    public String getStringHashForUser(String aUserName, String aPassWord) {
        String lFinalString = getStringForUser(aUserName, aPassWord);
        return hashBytes(lFinalString);
    }

    /**
     * apply hash algorithm to the byte array.
     */
    private String hashBytes(String aString) {
        String digest = getStringDigester().digest(aString);
        return digest;
    }

    /**
     * check if the hash value of the input is same as aHashedValue
     */
    public boolean compare(String aUserName, String aPassWord, String aHashedValue) {
        String lFinalString = getStringForUser(aUserName, aPassWord);
        return getStringDigester().matches(lFinalString, aHashedValue);
    }

    /**
     * get encryptor for this class.
     */
    private StandardStringDigester getStringDigester() {
        StandardStringDigester digester = new StandardStringDigester();
        digester.setSaltGenerator(new RandomSaltGenerator());
        digester.setAlgorithm(M_HASH_ALGO_NAME);
        digester.setIterations(M_NO_ITERATION);
        return digester;
    }

    /**
     * Get a string combination of username and pass.
     *
     * @param aUserName
     * @param aPassWord
     * @return
     * @throws
     */
    private String getStringForUser(String aUserName, String aPassWord) {

        if (aUserName == null || aPassWord == null) {
            return null;
        }
        // get byte array of all the strings.
        byte[] lUserArray = aUserName.getBytes();
        byte[] lPassArray = aPassWord.getBytes();
        byte[] lFinalString = new byte[lPassArray.length * 2];
        byte[] lTempArray = new byte[lPassArray.length];
        byte[] lStaticFields = M_STATIC_FIELDS.getBytes();

        if (lPassArray.length > lStaticFields.length) {
            // allocate new array.
            ByteArrayOutputStream lStream = new ByteArrayOutputStream();
            lStream.write(lStaticFields, 0, lStaticFields.length);
            int lDiff = lPassArray.length - lStaticFields.length;
            lStream.write(lPassArray, 0, lDiff);
            lStaticFields = lStream.toByteArray();
            try {
                lStream.close();
            }
            catch (IOException e) {
                logger.warn("getStringForUser failed due to " + e.getMessage());
            }
        }


        // populate temp array.
        for (int i = 0; i < lTempArray.length; i++) {
            if (i < lStaticFields.length) {
                if (i < lUserArray.length) {
                    if (i % 2 == 0) {
                        lTempArray[i] = (byte) (lUserArray[i] & lStaticFields[(lStaticFields.length - 1) - i]);
                    }
                    else {
                        lTempArray[i] = (byte) (lUserArray[i] | lStaticFields[(lStaticFields.length - 1) - i]);
                    }
                }
            }
            else {
                if (i % 2 == 0) {
                    lTempArray[i] = (byte) (lTempArray[(lTempArray.length - 1) - i] & lStaticFields[(lStaticFields.length - 1) - i]);
                }
                else {
                    lTempArray[i] = (byte) (lTempArray[(lTempArray.length - 1) - i] | lStaticFields[(lStaticFields.length - 1) - i]);
                }
            }
        }

        int j = 0;
        int k = 0;
        for (int i = 0; i < lFinalString.length; i++) {
            if (i % 2 == 0) {
                lFinalString[i] = lPassArray[j];
                j++;
            }
            else {
                lFinalString[i] = lTempArray[k];
                k++;
            }
        }

        String lResult = new String(Base64.encode(lFinalString));
        return lResult;
    }

    /**
     * Given an input string encrypt it and return the encrypted string.
     *
     * @param aInputString
     * @return
     */
    public String encryptString(String aInputString) {
        return getEncryptor().encrypt(aInputString);
    }

    /**
     * get Encryptor for this class.
     */
    private StandardPBEStringEncryptor getEncryptor() {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(M_STATIC_FIELDS);                     // we HAVE TO set a password
        encryptor.setAlgorithm(M_ALGO_NAME);    // optionally set the algorithm
        return encryptor;
    }

    /**
     * Decrypt the encrypted string .. if the string is not encrypted it would throw an error.
     *
     * @param aInputString
     * @return
     */
    public String decryptString(String aInputString) {
        return getEncryptor().decrypt(aInputString);
    }

    @Override
    public String hashString(String aInput) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean compare(String aInput, String aHashedValue) {
        throw new UnsupportedOperationException();
    }
}
