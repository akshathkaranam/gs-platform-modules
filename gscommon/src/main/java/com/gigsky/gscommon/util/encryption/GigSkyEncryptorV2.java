package com.gigsky.gscommon.util.encryption;


import org.jasypt.digest.StandardStringDigester;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.salt.RandomSaltGenerator;
import org.springframework.stereotype.Component;

@Component("V2_Encryptor")
public class GigSkyEncryptorV2 implements GigSkyEncryptor {

    public static final String M_VERSION = "1.0.1";
    public static final int M_NO_ITERATION = 1000;
    public static final String M_ALGO_NAME = "PBEWithMD5AndTripleDES";
    public static final String M_HASH_ALGO_NAME = "SHA-1";
    public static final String M_STATIC_FIELDS = "rKQ0z8xdVuqz8fgp0jqsOkAbgdnIHHVdXDKpmw==;";

    /**
     * print a byte array using characters.
     */
    private static void printByteArray(byte[] aBuffer) {
        for (int i = 0; i < aBuffer.length; i++) {
            System.out.println("\n" + " " + i + " char is=" + (char) aBuffer[i]);
        }
    }

    public String getStringHashForUser(String aUserName, String aPassWord) {
        return hashString(aPassWord);
    }

    /**
     * check if the hash value of the input is same as aHashedValue
     */
    public boolean compare(String aUserName, String aPassWord, String aHashedValue) {
        return getStringDigester().matches(aPassWord, aHashedValue);
    }

    /**
     * get encryptor for this class.
     */
    private StandardStringDigester getStringDigester() {
        StandardStringDigester digester = new StandardStringDigester();
        digester.setSaltGenerator(new RandomSaltGenerator());
        digester.setAlgorithm(M_HASH_ALGO_NAME);
        digester.setIterations(M_NO_ITERATION);
        return digester;
    }

    /**
     * Given an input string encrypt it and return the encrypted string.
     */
    public String encryptString(String aInputString) {
        return getEncryptor().encrypt(aInputString);
    }

    /**
     * get encryptor for this class.
     */
    private StandardPBEStringEncryptor getEncryptor() {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(M_STATIC_FIELDS);                     // we HAVE TO set a password
        encryptor.setAlgorithm(M_ALGO_NAME);    // optionally set the algorithm
        return encryptor;
    }

    /**
     * Decrypt the encrypted string .. if the string is not encrypted it would throw an error.
     */
    public String decryptString(String aInputString) {
        return getEncryptor().decrypt(aInputString);
    }

    @Override
    public String hashString(String aInput) {
        String digest = getStringDigester().digest(aInput);
        return digest;
    }

    @Override
    public boolean compare(String aInput, String aHashedValue) {
        return getStringDigester().matches(aInput, aHashedValue);
    }

    public static void main(String[] args) {
        System.out.println(new GigSkyEncryptorV2().decryptString("fQKpbo/Ua0Wgtk0c8e7J4XHRRgE5yeyQ"));
        System.out.println(new GigSkyEncryptorV2().encryptString("user"));
        System.out.println(new GigSkyEncryptorV2().encryptString("password"));
    }
}
